package siample.dev.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import siample.dev.domain.Story;
import siample.dev.service.StoryService;

@Controller
public class HomeController {
	
	@Autowired
	StoryService storyService;
	
	@RequestMapping("/")
	public String index(Model model) {
		model.addAttribute("stories", getStories());
		return "stories";
	}

	private List<Story> getStories() {
		//List<Story> stories = storyService.;
		return storyService.getStories();
	}
	

}
